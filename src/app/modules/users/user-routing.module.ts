import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UsersComponent  } from './components/users/users.component';
import { UserComponent  } from './components/user/user.component';
import { ExceptionsComponent  } from './components/user/exceptions.component';

const appRoutes: Routes = [
    { path: '', component: UsersComponent },
    { path: 'users', component: UsersComponent },
    { path: 'users/:name', component: UserComponent },
    { path: '**', component: ExceptionsComponent, pathMatch: 'full' }
];

@NgModule({
    imports: [
        RouterModule.forChild(appRoutes)
    ],
    exports: [
        RouterModule
    ]
})


export class UserRoutingModule {}