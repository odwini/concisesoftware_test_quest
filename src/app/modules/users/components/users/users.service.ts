import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class ContentService {
    public Content: any;

    constructor(private httpClient: HttpClient) { }

    getServices(): any {
        let url = "https://randomuser.me/api/?results=50";
        let headers = new HttpHeaders();
        let params: HttpParams = new HttpParams();
        params = params.set("results", "50");

        return this.httpClient.get(url, { headers, params });
    }
} 